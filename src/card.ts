import { Component } from './component';

export class Card extends Component {
    
    constructor(data: any, opts: any = {}) {
        super(data);
        
        // If the user passed a single non-object
        if (typeof data !== 'object') {
            var str = data.toString();
            data = {
                title: str
            };

            // If the user passed a list
        } else if (Array.isArray(data)) {
            data = {
                title: data[0] || '',
                content: data[1] || '',
                subtitle: data[2] || '',
                image: data[3] || ''
            };

        } // Otherwise keep item object as-is

        // Initialize component variables
        let elemClasses = (opts.classes || []);

        // Build the primary section
        let title = `<h1 class="mdc-card__title mdc-card__title--large">${data.title}</h1>`;
        let subtitle = '';
        if (data.subtitle) {
            subtitle = `<h2 class="mdc-card__subtitle">${data.subtitle}</h2>`;
        }
        let primary = `<section class="mdc-card__primary">${title}${subtitle}</section>`;
        
        // Build the actions section
        // TODO: use Buttons elements
        var eventBinders: any = [];
        let actions = `<section class="mdc-card__actions">` + 
            (data.actions || [{label: 'OK'}]).map((action: any, ix: number, arr: any[]) => {
                let attrs = '';
                
                // If user provided onclick callback, bind to the event
                if (action.onclick) {
                    let onclick: any;
                    let id = `card-action-${ix}-${Component.id()}`;
                    attrs = `id="${id}"`;
                    if (opts.autodismiss) {
                        onclick = function(event: any) {
                            action.onclick(event);
                            this.parentNode.parentNode.remove();
                        };
                    } else {
                        onclick = action.onclick;
                    }
                    
                    // Attach the onclick handlers upon creating html element
                    eventBinders.push(function(card: Element) {
                        card.querySelector('#' + id).addEventListener('click', onclick);
                    });
                    
                // Autodismiss can be implemented using just a string
                } else if (action.autodismiss || (opts.autodismiss && ix === arr.length -1)) {
                    attrs += ` onclick="this.parentNode.parentNode.remove();"`;
                }

                if (action.blank) {
                    attrs += ` target="_blank"`;
                }
                return `<a href="${action.href || 'javascript:void(0);'}" ${attrs}>
                            <button class="mdc-button mdc-button--compact mdc-card__action">
                                ${action.label}
                            </button>
                        </a>`;
            }).join('') + `</section>`;
        
        // Bind all onclick events as soon as the element is created
        this.postExecute = function(elem: Element) {
            eventBinders.forEach((binder: any) => { binder(elem); });
        };
        
        // Build the optional media section
        let media = '';
        if (data.image) {
            //media = `<img class="mdc-card__media-item mdc-card__media-item--1x" src="${data.image}"></img>`;
            media = `<section class="mdc-card__media" style="background-image:url('${data.image}');"></section>`
        }
        
        // Build the optional content section
        let content = '';
        if (data.content) {
            const contentClasses = ['mdc-card__supporting-text'];
            if (data.image) {
                contentClasses.push('mdc-card__supporting-text--with-media')
            }
            content = `<section class="${contentClasses.join(' ')}">${data.content}</section>`;
        }
        
		this.str = `<div class="uitoolkit-card mdc-card ${elemClasses.join(' ')}">${primary}${media}${content}${actions}</div>`;
	}
}