import { Component } from './component';

export class Radios extends Component {
    
    constructor(data: any[], opts: any = {}) {
        super(data);
        
        let name = opts.name;
        let radioGroupClasses: {[key:string]:number} = {};
        //radioGroupClasses['uitoolkit-carousel'] = 0;
        
        let inner: string = data.map((item: any, ix: number): string => {

        	// If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    label: item.toString()
                };

            // If the user passed a list of arrays
            } else if (Array.isArray(item)) {
                item = {
                    label: item[0],
                    value: item.length > 1 ? item[1] : '',
                    id: item.length > 2 ? item[2] : ''
                };

            } // Otherwise keep item object as-is

        	// If name was not given, use the first element's label or fallback
        	// to random id if all else fails
        	name = name || item.label || Component.id();
        
        	// If value is not given, use the element's label
            let value = item.value || item.label;
        
        	// If id is not given, make one up
        	let id = item.id || Component.id();

        	// Initialize item variables
        	let post = '';
        	let radioStyles: {[key:string]:number} = {};
        	let radioClasses: {[key:string]:number} = {};
        
        	// Build the template from all the options parsed
        	let label = '';
        	if (item.label) {
                label = `<label for="${id}">${item.label}</label>`;
            }
            return `<div class="mdc-form-field">
						<div class="uitoolkit-radio mdc-radio">
                            <input class="mdc-radio__native-control" type="radio" id="${id}" name="${name}" value="${value}"
                                   ${opts.required || item.required ? 'required' : ''} ${item.checked ? 'checked' : ''}/>
                            <div class="mdc-radio__background">
                                <div class="mdc-radio__outer-circle"></div>
                                <div class="mdc-radio__inner-circle"></div>
                            </div>
						</div>
	                    ${label}
                    </div>`;
    	}).join('');

		this.str = `${inner}`;
	}
}