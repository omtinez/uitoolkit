import { Component } from './component';

export class List extends Component {
    
    constructor(data: any[], opts: any = {}) {
        super(data);
        
		let attrs: string[] = [];
		let eventBinders: any = [];
		let ulClasses: string[] = (opts.ulclasses || []);
        if (opts.dense) ulClasses.push('mdc-list--dense');
        if (opts.avatars) ulClasses.push('mdc-list--avatar-list');
        
        let inner: string = data.map((item: any, ix: number): string => {

        	// If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = { primary: [item.toString()] };

            // If the user passed a list of arrays
            } else if (Array.isArray(item)) {
                item = { primary: item };

            } // Otherwise keep item object as-is
        
        	// Initialize item variables
        	let pre = '';
        	let post = '';
        	let labels: any[];
        	let liClasses: string[] = (opts.liclasses || []);
        
        	// Should the list elements be clickable?
        	if (item.href || item.onclick) {
                liClasses.push('uitoolkit-clickable');
                post += `<a class="uitoolkit-clickable" href="${item.href || 'javascript:void(0);'}"></a>`;
            }
        
        	// Iterate over all primary text labels
        	labels = Array.isArray(item.primary) ? item.primary : [item.primary];
        	let primary = labels.map((label:string) => {
            	return `<span class="mdc-list-item__text__primary">${label}</span>`;
            }).join('');
        	if (labels.length > 0) {
                ulClasses.push('mdc-list--two-line');
            }
        
        	// Iterate over all secondary text labels
        	let secondary = '';
        	if (item.secondary) {
                labels = Array.isArray(item.secondary) ? item.secondary : [item.secondary];
                secondary = '<br/>' + labels.map((label:string) => {
            		return `<span class="mdc-list-item__text__secondary">${label}</span>`;
            	}).join('');
                ulClasses.push('mdc-list--two-line');
            }
        
        	// Did user provide image?
        	if (item.image) {
				liClasses.push('uitoolkit-list-li--image');
                pre += `<img class="mdc-list-item__start-detail" src="${item.image}"
							width="48" height="48" alt="${item.image_alt || ''}">`;
            }

			// Attach the onclick handlers upon creating html element
			if (item.onclick) {
				eventBinders.push(function(elem: Element) {
					// The browser might not have indexed nodes by id yet, so we need to go by index
					elem.children[ix].addEventListener('click', item.onclick);
				});
			}

        	return `<li class="uitoolkit-list-li mdc-list-item ${liClasses.join(' ')}" ${attrs.join(' ')} data-index="${ix}">
						${pre}
						<span class="mdc-list-item__text">${primary}${secondary}</span>
						${post}
					</li>`;
    	}).join('');

        // Bind all onclick events as soon as the element is created
        this.postExecute = function(elem: Element) {
            eventBinders.forEach((binder: any) => { binder(elem); });
        };

		this.str = `<ul class="uitoolkit-list-ul mdc-list ${ulClasses.join(' ')}">${inner}</ul>`;
	}
}