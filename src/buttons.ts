import { Component } from './component';

export class Buttons extends Component {
    
    constructor(data: any[], opts: any = {}) {
        super(data);
        
        var eventBinders: any = [];
        let inner: string = data.map((item: any, ix: number): string => {

            // If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    label: item.toString()
                };

            // If the user passed a list of arrays
            } else if (Array.isArray(item)) {
                item = {
                    label: item[0],
                    href: item.length > 1 ? item[1] : '',
                    id: item.length > 2 ? item[2] : ''
                };

            } // Otherwise keep item object as-is

            // Initialize item variables
            let attrs = '';

            // Did the user specify an id?
            if (item.id) {
                attrs += ` id="${item.id}"`;
            }

            // Parse the onclick callback attached to this button if any
            if (item.onclick) {
                let onclick: any;
                onclick = item.onclick;
                
                // Attach the onclick handlers upon creating html element
                eventBinders.push(function(elem: Element) {
                    // The browser might not have indexed nodes by id yet, so we need to go by index
                    elem.children[ix].addEventListener('click', item.onclick);
                });
            }

            // Initialize item variables
            let post = '';
            let elemStyles: {[key:string]:number} = {};
            (opts.styles || []).concat(item.styles || []).forEach(function(style: string) {
                elemStyles[style] = 0;
            });
            let elemClasses: {[key:string]:number} = {};
            (opts.classes || []).concat(item.classes || []).forEach(function(klass: string) {
                elemClasses[klass] = 0;
            });

            // Parse the different options for this item
            if (opts.colored || item.colored) {
                elemClasses['mdc-button--accent'] = 0;
            }
            if (opts.raised || item.raised) {
                elemClasses['mdc-button--raised'] = 0;
            }
            if (opts.disabled || item.disabled) {
                attrs += ' disabled';
            }

            // Border defaults to true
            if (item.borderless || opts.borderless) {
                elemClasses['borderless'] = 0;
            }
       
            // Build the template from all the options parsed
            return `<a class="uitoolkit-button--wrapper" href="${item.href || 'javascript:void(0);'}">
                        <button
                            style="${Object.keys(elemStyles).join('; ')}"
                            class="uitoolkit-button mdc-button mdc-button--compact ${Object.keys(elemClasses).join(' ')}"
                            ${attrs}>
                            ${item.label}
                        </button>
                    </a>`;
        }).join('');

        // Bind all onclick events as soon as the element is created
        this.postExecute = function(elem: Element) {
            eventBinders.forEach((binder: any) => { binder(elem); });
        };

        this.str = `${inner}`;
    }
}