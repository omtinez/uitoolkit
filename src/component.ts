export class Component {
    protected str: string;
	public postExecute: ((elem: Element) => void);

	constructor(data: any) {
        this.str = data.toString();
    }

	public static id(len: number = 8): string {
        let key = '';
        for (let i = 0; i < len; i++) {
            key += String.fromCharCode(65 + Math.floor(Math.random() * 26));
        }
        return key;
    }
        

    public html(): Node|Node[] {
        const d = document as any;
        const div: HTMLElement = d.createElement('div');
        div.innerHTML = this.str.trim();
        const children = [].slice.call(div.childNodes);
        if (this.postExecute) children.forEach(this.postExecute);
        return children.length > 1 ? children : children[0];
    }

    public toString(): string {
        return this.str;
    }
}

