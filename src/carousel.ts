import { Component } from './component';

/**
 * Linear grid of images in the form of a carouselk. Supports top and bottom text in each tile as
 * well as custom click behavior.
 */
export class Carousel extends Component {
    
    /**
     * 
     * @param data List of image URLs or {image: URL, text-top: header, text-bottom: footer}.
     * Optional keys are: {href: URL, alt: text}
     * @param opts 
     */
    constructor(data: any[], opts: any = {}) {
        super(data);
        
        let elemClasses: {[key:string]:number} = {};
        elemClasses['uitoolkit-carousel'] = 0;
        
        let inner: string = data.map((item: any, ix: number): string => {

        	// If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    image: item.toString()
                };

            } // Otherwise keep item object as-is
        
        	// Initialize item variables
        	let post = '';
        	let imageStyles: {[key:string]:number} = {};
        	let imageClasses: {[key:string]:number} = {};
            imageClasses['uitoolkit-carousel--img'] = 0;
        
        	// Should the tile elements be clickable?
        	if (item.href) {
                post += `<a class="uitoolkit-clickable" href="${item.href}"></a>`;
            }
        
        	// Add tile text top
        	let textTop = '';
        	if (item['text-top']) {
                textTop = `<span class="uitoolkit-grid-cell--top">${item['text-top']}</span>`
            }
        
        	// Add tile text bottom
        	let textBottom = '';
        	if (item['text-bottom']) {
                textBottom = `<span class="uitoolkit-grid-cell--bottom">${item['text-bottom']}</span>`
            }
        
        	return `<div class="uitoolkit-carousel--wrapper">
						${textTop}${textBottom}
						<img class="${Object.keys(imageClasses).join(' ')}"
                             style="${Object.keys(imageStyles).join('; ')}" src="${item.image}"
                             alt="${item.alt || ''}" />
						${post}
					</div>`;
    	}).join('');

		this.str = `<div class="uitoolkit-carousel ${Object.keys(elemClasses).join(' ')}">${inner}</div>`;
	}
}