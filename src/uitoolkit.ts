import * as UIToolkit from './combined';

// Insert into global scope
if (typeof(window) !== 'undefined') {
    const w = window as any;
    w.UIToolkit = UIToolkit;
}

export { UIToolkit };