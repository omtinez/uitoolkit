import { Component } from './component';
import { Card } from './card';

export class PopUp extends Component {
    
    constructor(data: any, opts: any = {}) {
        super(data);

        // Initialize component variables
        let elemClasses = (opts.classes || []);
        
        // The data goes as-is to the card constructor
        let cardOpts = (Object as any).assign({}, opts);
        cardOpts.classes = ['uitoolkit-popup--card'];
        let card = new Card(data, cardOpts);
        
        // Parse the options for the configurable parts of popups
        let onclick = '';
        let blanketClasses: string[] = [];
        if (opts.dim) blanketClasses.push('dim');
        if (opts.autoclose) {
            blanketClasses.push('closeable');
            onclick = 'this.parentNode.remove();';
        }
        
        // Parent listens to all click events and after 0.1 seconds dismisses popup if card was dismissed
        this.postExecute = function(elem: Element) {
            card.postExecute(elem);
            elem.addEventListener('click', function(event) {
                (window as any).setTimeout(() => {
                    if (elem.querySelectorAll('.uitoolkit-popup--card').length === 0) {
                        elem.remove();
                    }
                }, 100);
            });
        };
        
        // This class only creates the "blanket" underneath the card
        this.str = `<div class="uitoolkit-popup ${elemClasses.join(' ')}">
						<div class="uitoolkit-popup--background ${blanketClasses.join(' ')}" onclick="${onclick}"></div>
						${card.toString()}
					</div>`;
	}
}