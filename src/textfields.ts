import { Component } from './component';

export class Textfields extends Component {
    
    constructor(data: any[], opts: any = {}) {
        super(data);
        
        let inner: string = data.map((item: any, ix: number): string => {

        	// If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    name: item.toString()
                };

            } // Otherwise keep item object as-is

        	// If name is not given, use the element's label
            let name = item.name || item.label;
        
        	// If id is not given, make one up
        	let id = item.id || Component.id();

        	// Initialize item variables
        	let post = '';
            let attrs: {[key:string]:number} = {};
        	let elemStyles: {[key:string]:number} = {};
            (opts.styles || []).concat(item.styles || []).forEach(function(style: string) {
                elemStyles[style] = 0;
            });
        	let elemClasses: {[key:string]:number} = {};
            (opts.classes || []).concat(item.classes || []).forEach(function(klass: string) {
                elemClasses[klass] = 0;
            });
        
        	// Build the template from all the options parsed
        	let label = '';
        	if (item.label || opts.autolabel) {
                label = `<label for="${id}" class="uitoolkit-textfield--label">${item.label || item.name}</label>`;
            }
        	if (!item.nohint && !opts.nohint) {
                attrs[`placeholder="${item.hint || name}"`] = 0;
            }
            if (item.required || opts.required) {
                attrs['required'] = 0;
            }
            if (item.disabled || opts.disabled) {
                attrs['disabled'] = 0;
            }
            if (item.pattern || opts.pattern) {
                attrs[`pattern="${item.pattern || opts.pattern}"`] = 0;
            }
            if (item.autocomplete || opts.autocomplete) {
                attrs[`autocomplete="${item.autocomplete || opts.autocomplete}"`] = 0;
            }
            let input = '';
            if (opts.multiline || item.multiline) {
                elemClasses['mdc-textfield--multiline'] = 0;
                input = `<textarea id="${id}" name="${name}" class="mdc-textfield__input"
                                   ${Object.keys(attrs).join(' ')} rows="${item.rows || 8}" 
                                   cols="${item.cols || 40}"></textarea>`;
            } else {
                input = `<input type="${item.type || 'text'}" id="${id}" name="${name}"
                                ${Object.keys(attrs).join(' ')} class="mdc-textfield__input" />`;

            }
        
        	return `<div class="mdc-form-field ${Object.keys(elemClasses).join(' ')}" style="${Object.keys(elemStyles).join('; ')}">
                        ${label}
                        <div class="mdc-textfield ${opts.multiline || item.multiline ? '' : 'mdc-textfield--fullwidth'}">
                            ${input}
                        </div>
                    </div>`;
    	}).join('');

		this.str = `${inner}`;
	}
}