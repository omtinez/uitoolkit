var gulp = require('gulp');
var ts = require('gulp-typescript');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var browserify = require('gulp-browserify');

// Clean tasks

gulp.task('clean', function() {
    return gulp.src('dist', { read: false })
    	.pipe(clean());
});


// Build tasks

gulp.task('ts', function() {
    return gulp.src('src/**/*.ts')
        .pipe(ts({
        	target: 'ES5',
            declaration: true,
            module: 'commonjs',
            noImplicitAny: true,
        	skipLibCheck: true
         }))
    	.pipe(gulp.dest('dist'));
});

gulp.task('css', function () {
    return gulp.src(['src/**/*.css'])
        .pipe(concat('uitoolkit.css'))
        .pipe(gulp.dest('dist'));
});

gulp.task('copy', function () {
    return gulp.src(['src/**/*', '!src/**/*.ts', '!src/**/*.css', '!src/**/*.sass'])
        .pipe(gulp.dest('dist'));
});

gulp.task('uglify', function() {
    return gulp.src('dist/uitoolkit.js')
        .pipe(browserify({
          insertGlobals : true,
        }))
    	.pipe(uglify())
        .pipe(rename({suffix: '.min'}))
    	.pipe(gulp.dest('dist'));
});

gulp.task('build', gulp.series('ts', 'css', 'copy', 'uglify'));
gulp.task('default', gulp.series('build'));
