"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UIToolkit = require("./combined");
exports.UIToolkit = UIToolkit;
// Insert into global scope
if (typeof (window) !== 'undefined') {
    var w = window;
    w.UIToolkit = UIToolkit;
}
