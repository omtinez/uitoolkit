"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
var Card = (function (_super) {
    __extends(Card, _super);
    function Card(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        // If the user passed a single non-object
        if (typeof data !== 'object') {
            var str = data.toString();
            data = {
                title: str
            };
            // If the user passed a list
        }
        else if (Array.isArray(data)) {
            data = {
                title: data[0] || '',
                content: data[1] || '',
                subtitle: data[2] || '',
                image: data[3] || ''
            };
        } // Otherwise keep item object as-is
        // Initialize component variables
        var elemClasses = (opts.classes || []);
        // Build the primary section
        var title = "<h1 class=\"mdc-card__title mdc-card__title--large\">" + data.title + "</h1>";
        var subtitle = '';
        if (data.subtitle) {
            subtitle = "<h2 class=\"mdc-card__subtitle\">" + data.subtitle + "</h2>";
        }
        var primary = "<section class=\"mdc-card__primary\">" + title + subtitle + "</section>";
        // Build the actions section
        // TODO: use Buttons elements
        var eventBinders = [];
        var actions = "<section class=\"mdc-card__actions\">" +
            (data.actions || [{ label: 'OK' }]).map(function (action, ix, arr) {
                var attrs = '';
                // If user provided onclick callback, bind to the event
                if (action.onclick) {
                    var onclick_1;
                    var id_1 = "card-action-" + ix + "-" + component_1.Component.id();
                    attrs = "id=\"" + id_1 + "\"";
                    if (opts.autodismiss) {
                        onclick_1 = function (event) {
                            action.onclick(event);
                            this.parentNode.parentNode.remove();
                        };
                    }
                    else {
                        onclick_1 = action.onclick;
                    }
                    // Attach the onclick handlers upon creating html element
                    eventBinders.push(function (card) {
                        card.querySelector('#' + id_1).addEventListener('click', onclick_1);
                    });
                    // Autodismiss can be implemented using just a string
                }
                else if (action.autodismiss || (opts.autodismiss && ix === arr.length - 1)) {
                    attrs += " onclick=\"this.parentNode.parentNode.remove();\"";
                }
                if (action.blank) {
                    attrs += " target=\"_blank\"";
                }
                return "<a href=\"" + (action.href || 'javascript:void(0);') + "\" " + attrs + ">\n                            <button class=\"mdc-button mdc-button--compact mdc-card__action\">\n                                " + action.label + "\n                            </button>\n                        </a>";
            }).join('') + "</section>";
        // Bind all onclick events as soon as the element is created
        _this.postExecute = function (elem) {
            eventBinders.forEach(function (binder) { binder(elem); });
        };
        // Build the optional media section
        var media = '';
        if (data.image) {
            //media = `<img class="mdc-card__media-item mdc-card__media-item--1x" src="${data.image}"></img>`;
            media = "<section class=\"mdc-card__media\" style=\"background-image:url('" + data.image + "');\"></section>";
        }
        // Build the optional content section
        var content = '';
        if (data.content) {
            var contentClasses = ['mdc-card__supporting-text'];
            if (data.image) {
                contentClasses.push('mdc-card__supporting-text--with-media');
            }
            content = "<section class=\"" + contentClasses.join(' ') + "\">" + data.content + "</section>";
        }
        _this.str = "<div class=\"uitoolkit-card mdc-card " + elemClasses.join(' ') + "\">" + primary + media + content + actions + "</div>";
        return _this;
    }
    return Card;
}(component_1.Component));
exports.Card = Card;
