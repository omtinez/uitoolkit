"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
var Textfields = (function (_super) {
    __extends(Textfields, _super);
    function Textfields(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        var inner = data.map(function (item, ix) {
            // If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    name: item.toString()
                };
            } // Otherwise keep item object as-is
            // If name is not given, use the element's label
            var name = item.name || item.label;
            // If id is not given, make one up
            var id = item.id || component_1.Component.id();
            // Initialize item variables
            var post = '';
            var attrs = {};
            var elemStyles = {};
            (opts.styles || []).concat(item.styles || []).forEach(function (style) {
                elemStyles[style] = 0;
            });
            var elemClasses = {};
            (opts.classes || []).concat(item.classes || []).forEach(function (klass) {
                elemClasses[klass] = 0;
            });
            // Build the template from all the options parsed
            var label = '';
            if (item.label || opts.autolabel) {
                label = "<label for=\"" + id + "\" class=\"uitoolkit-textfield--label\">" + (item.label || item.name) + "</label>";
            }
            if (!item.nohint && !opts.nohint) {
                attrs["placeholder=\"" + (item.hint || name) + "\""] = 0;
            }
            if (item.required || opts.required) {
                attrs['required'] = 0;
            }
            if (item.disabled || opts.disabled) {
                attrs['disabled'] = 0;
            }
            if (item.pattern || opts.pattern) {
                attrs["pattern=\"" + (item.pattern || opts.pattern) + "\""] = 0;
            }
            if (item.autocomplete || opts.autocomplete) {
                attrs["autocomplete=\"" + (item.autocomplete || opts.autocomplete) + "\""] = 0;
            }
            var input = '';
            if (opts.multiline || item.multiline) {
                elemClasses['mdc-textfield--multiline'] = 0;
                input = "<textarea id=\"" + id + "\" name=\"" + name + "\" class=\"mdc-textfield__input\"\n                                   " + Object.keys(attrs).join(' ') + " rows=\"" + (item.rows || 8) + "\" \n                                   cols=\"" + (item.cols || 40) + "\"></textarea>";
            }
            else {
                input = "<input type=\"" + (item.type || 'text') + "\" id=\"" + id + "\" name=\"" + name + "\"\n                                " + Object.keys(attrs).join(' ') + " class=\"mdc-textfield__input\" />";
            }
            return "<div class=\"mdc-form-field " + Object.keys(elemClasses).join(' ') + "\" style=\"" + Object.keys(elemStyles).join('; ') + "\">\n                        " + label + "\n                        <div class=\"mdc-textfield " + (opts.multiline || item.multiline ? '' : 'mdc-textfield--fullwidth') + "\">\n                            " + input + "\n                        </div>\n                    </div>";
        }).join('');
        _this.str = "" + inner;
        return _this;
    }
    return Textfields;
}(component_1.Component));
exports.Textfields = Textfields;
