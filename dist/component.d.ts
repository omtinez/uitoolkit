export declare class Component {
    protected str: string;
    postExecute: ((elem: Element) => void);
    constructor(data: any);
    static id(len?: number): string;
    html(): Node | Node[];
    toString(): string;
}
