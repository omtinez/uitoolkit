"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
var Radios = (function (_super) {
    __extends(Radios, _super);
    function Radios(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        var name = opts.name;
        var radioGroupClasses = {};
        //radioGroupClasses['uitoolkit-carousel'] = 0;
        var inner = data.map(function (item, ix) {
            // If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    label: item.toString()
                };
                // If the user passed a list of arrays
            }
            else if (Array.isArray(item)) {
                item = {
                    label: item[0],
                    value: item.length > 1 ? item[1] : '',
                    id: item.length > 2 ? item[2] : ''
                };
            } // Otherwise keep item object as-is
            // If name was not given, use the first element's label or fallback
            // to random id if all else fails
            name = name || item.label || component_1.Component.id();
            // If value is not given, use the element's label
            var value = item.value || item.label;
            // If id is not given, make one up
            var id = item.id || component_1.Component.id();
            // Initialize item variables
            var post = '';
            var radioStyles = {};
            var radioClasses = {};
            // Build the template from all the options parsed
            var label = '';
            if (item.label) {
                label = "<label for=\"" + id + "\">" + item.label + "</label>";
            }
            return "<div class=\"mdc-form-field\">\n\t\t\t\t\t\t<div class=\"uitoolkit-radio mdc-radio\">\n                            <input class=\"mdc-radio__native-control\" type=\"radio\" id=\"" + id + "\" name=\"" + name + "\" value=\"" + value + "\"\n                                   " + (opts.required || item.required ? 'required' : '') + " " + (item.checked ? 'checked' : '') + "/>\n                            <div class=\"mdc-radio__background\">\n                                <div class=\"mdc-radio__outer-circle\"></div>\n                                <div class=\"mdc-radio__inner-circle\"></div>\n                            </div>\n\t\t\t\t\t\t</div>\n\t                    " + label + "\n                    </div>";
        }).join('');
        _this.str = "" + inner;
        return _this;
    }
    return Radios;
}(component_1.Component));
exports.Radios = Radios;
