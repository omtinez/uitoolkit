"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
/**
 * Linear grid of images in the form of a carouselk. Supports top and bottom text in each tile as
 * well as custom click behavior.
 */
var Carousel = (function (_super) {
    __extends(Carousel, _super);
    /**
     *
     * @param data List of image URLs or {image: URL, text-top: header, text-bottom: footer}.
     * Optional keys are: {href: URL, alt: text}
     * @param opts
     */
    function Carousel(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        var elemClasses = {};
        elemClasses['uitoolkit-carousel'] = 0;
        var inner = data.map(function (item, ix) {
            // If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    image: item.toString()
                };
            } // Otherwise keep item object as-is
            // Initialize item variables
            var post = '';
            var imageStyles = {};
            var imageClasses = {};
            imageClasses['uitoolkit-carousel--img'] = 0;
            // Should the tile elements be clickable?
            if (item.href) {
                post += "<a class=\"uitoolkit-clickable\" href=\"" + item.href + "\"></a>";
            }
            // Add tile text top
            var textTop = '';
            if (item['text-top']) {
                textTop = "<span class=\"uitoolkit-grid-cell--top\">" + item['text-top'] + "</span>";
            }
            // Add tile text bottom
            var textBottom = '';
            if (item['text-bottom']) {
                textBottom = "<span class=\"uitoolkit-grid-cell--bottom\">" + item['text-bottom'] + "</span>";
            }
            return "<div class=\"uitoolkit-carousel--wrapper\">\n\t\t\t\t\t\t" + textTop + textBottom + "\n\t\t\t\t\t\t<img class=\"" + Object.keys(imageClasses).join(' ') + "\"\n                             style=\"" + Object.keys(imageStyles).join('; ') + "\" src=\"" + item.image + "\"\n                             alt=\"" + (item.alt || '') + "\" />\n\t\t\t\t\t\t" + post + "\n\t\t\t\t\t</div>";
        }).join('');
        _this.str = "<div class=\"uitoolkit-carousel " + Object.keys(elemClasses).join(' ') + "\">" + inner + "</div>";
        return _this;
    }
    return Carousel;
}(component_1.Component));
exports.Carousel = Carousel;
