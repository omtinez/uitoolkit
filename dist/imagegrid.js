"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
/**
 * Grid of images in the form of tiles. Supports top and bottom text in each tile as well as custom
 * click behavior.
 */
var ImageGrid = (function (_super) {
    __extends(ImageGrid, _super);
    /**
     * Initialize a Component for a grid of images in the form of tiles.
     * @param data List of image URLs or {image: URL, text-top: header, text-bottom: footer}.
     * Optional keys are: {href: URL, height: pixels}
     * @param opts
     */
    function ImageGrid(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        var gridClasses = {};
        //let gridBaseClasses: {[key:string]:bool} = {};
        var inner = data.map(function (item, ix) {
            // If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    image: item.toString()
                };
            } // Otherwise keep item object as-is
            // Initialize item variables
            var post = '';
            var gridStyles = {};
            var gridClasses = {};
            if (item.span) {
                gridClasses['mdc-layout-grid__cell--span-' + item.span] = 0;
            }
            else {
                gridClasses['mdc-layout-grid__cell--span-4'] = 0; // 3 per row
                gridClasses['mdc-layout-grid__cell--span-4-tablet'] = 0; // 2 per row
                gridClasses['mdc-layout-grid__cell--span-4-phone'] = 0; // 1 per row
            }
            // Should the grid elements be clickable?
            if (item.href) {
                post += "<a class=\"uitoolkit-clickable\" href=\"" + item.href + "\"></a>";
            }
            // Does the grid have a specific height requirement?
            var height = 224 || item.height; // TODO: Magic / default numbers should be defined symbolically...
            gridStyles["height:" + height + "px"] = 0;
            // Add the background image
            var img = '';
            if (item.image) {
                gridStyles["background-image:url('" + item.image + "')"] = 0;
            }
            // Add grid text top
            var textTop = '';
            if (item['text-top']) {
                textTop = "<span class=\"uitoolkit-grid-cell--top\">" + item['text-top'] + "</span>";
            }
            // Add grid text bottom
            var textBottom = '';
            if (item['text-bottom']) {
                textBottom = "<span class=\"uitoolkit-grid-cell--bottom\">" + item['text-bottom'] + "</span>";
            }
            return "\n                <div class=\"uitoolkit-grid-cell mdc-layout-grid__cell\n                     " + Object.keys(gridClasses).join(' ') + "\"\n                     style=\"" + Object.keys(gridStyles).join('; ') + "\">\n                         " + textTop + textBottom + post + "\n                </div>";
        }).join('');
        _this.str = "\n            <div class=\"uitoolkit-grid mdc-layout-grid " + Object.keys(gridClasses).join(' ') + "\">\n                <div class=\"mdc-layout-grid__inner\">\n                    " + inner + "\n                </div>\n            </div>";
        return _this;
    }
    return ImageGrid;
}(component_1.Component));
exports.ImageGrid = ImageGrid;
