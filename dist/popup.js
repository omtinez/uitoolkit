"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
var card_1 = require("./card");
var PopUp = (function (_super) {
    __extends(PopUp, _super);
    function PopUp(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        // Initialize component variables
        var elemClasses = (opts.classes || []);
        // The data goes as-is to the card constructor
        var cardOpts = Object.assign({}, opts);
        cardOpts.classes = ['uitoolkit-popup--card'];
        var card = new card_1.Card(data, cardOpts);
        // Parse the options for the configurable parts of popups
        var onclick = '';
        var blanketClasses = [];
        if (opts.dim)
            blanketClasses.push('dim');
        if (opts.autoclose) {
            blanketClasses.push('closeable');
            onclick = 'this.parentNode.remove();';
        }
        // Parent listens to all click events and after 0.1 seconds dismisses popup if card was dismissed
        _this.postExecute = function (elem) {
            card.postExecute(elem);
            elem.addEventListener('click', function (event) {
                window.setTimeout(function () {
                    if (elem.querySelectorAll('.uitoolkit-popup--card').length === 0) {
                        elem.remove();
                    }
                }, 100);
            });
        };
        // This class only creates the "blanket" underneath the card
        _this.str = "<div class=\"uitoolkit-popup " + elemClasses.join(' ') + "\">\n\t\t\t\t\t\t<div class=\"uitoolkit-popup--background " + blanketClasses.join(' ') + "\" onclick=\"" + onclick + "\"></div>\n\t\t\t\t\t\t" + card.toString() + "\n\t\t\t\t\t</div>";
        return _this;
    }
    return PopUp;
}(component_1.Component));
exports.PopUp = PopUp;
