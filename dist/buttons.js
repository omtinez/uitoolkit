"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
var Buttons = (function (_super) {
    __extends(Buttons, _super);
    function Buttons(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        var eventBinders = [];
        var inner = data.map(function (item, ix) {
            // If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = {
                    label: item.toString()
                };
                // If the user passed a list of arrays
            }
            else if (Array.isArray(item)) {
                item = {
                    label: item[0],
                    href: item.length > 1 ? item[1] : '',
                    id: item.length > 2 ? item[2] : ''
                };
            } // Otherwise keep item object as-is
            // Initialize item variables
            var attrs = '';
            // Did the user specify an id?
            if (item.id) {
                attrs += " id=\"" + item.id + "\"";
            }
            // Parse the onclick callback attached to this button if any
            if (item.onclick) {
                var onclick_1;
                onclick_1 = item.onclick;
                // Attach the onclick handlers upon creating html element
                eventBinders.push(function (elem) {
                    // The browser might not have indexed nodes by id yet, so we need to go by index
                    elem.children[ix].addEventListener('click', item.onclick);
                });
            }
            // Initialize item variables
            var post = '';
            var elemStyles = {};
            (opts.styles || []).concat(item.styles || []).forEach(function (style) {
                elemStyles[style] = 0;
            });
            var elemClasses = {};
            (opts.classes || []).concat(item.classes || []).forEach(function (klass) {
                elemClasses[klass] = 0;
            });
            // Parse the different options for this item
            if (opts.colored || item.colored) {
                elemClasses['mdc-button--accent'] = 0;
            }
            if (opts.raised || item.raised) {
                elemClasses['mdc-button--raised'] = 0;
            }
            if (opts.disabled || item.disabled) {
                attrs += ' disabled';
            }
            // Border defaults to true
            if (item.borderless || opts.borderless) {
                elemClasses['borderless'] = 0;
            }
            // Build the template from all the options parsed
            return "<a class=\"uitoolkit-button--wrapper\" href=\"" + (item.href || 'javascript:void(0);') + "\">\n                        <button\n                            style=\"" + Object.keys(elemStyles).join('; ') + "\"\n                            class=\"uitoolkit-button mdc-button mdc-button--compact " + Object.keys(elemClasses).join(' ') + "\"\n                            " + attrs + ">\n                            " + item.label + "\n                        </button>\n                    </a>";
        }).join('');
        // Bind all onclick events as soon as the element is created
        _this.postExecute = function (elem) {
            eventBinders.forEach(function (binder) { binder(elem); });
        };
        _this.str = "" + inner;
        return _this;
    }
    return Buttons;
}(component_1.Component));
exports.Buttons = Buttons;
