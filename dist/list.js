"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
var List = (function (_super) {
    __extends(List, _super);
    function List(data, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, data) || this;
        var attrs = [];
        var eventBinders = [];
        var ulClasses = (opts.ulclasses || []);
        if (opts.dense)
            ulClasses.push('mdc-list--dense');
        if (opts.avatars)
            ulClasses.push('mdc-list--avatar-list');
        var inner = data.map(function (item, ix) {
            // If the user passed a list of non-objects
            if (typeof item !== 'object') {
                item = { primary: [item.toString()] };
                // If the user passed a list of arrays
            }
            else if (Array.isArray(item)) {
                item = { primary: item };
            } // Otherwise keep item object as-is
            // Initialize item variables
            var pre = '';
            var post = '';
            var labels;
            var liClasses = (opts.liclasses || []);
            // Should the list elements be clickable?
            if (item.href || item.onclick) {
                liClasses.push('uitoolkit-clickable');
                post += "<a class=\"uitoolkit-clickable\" href=\"" + (item.href || 'javascript:void(0);') + "\"></a>";
            }
            // Iterate over all primary text labels
            labels = Array.isArray(item.primary) ? item.primary : [item.primary];
            var primary = labels.map(function (label) {
                return "<span class=\"mdc-list-item__text__primary\">" + label + "</span>";
            }).join('');
            if (labels.length > 0) {
                ulClasses.push('mdc-list--two-line');
            }
            // Iterate over all secondary text labels
            var secondary = '';
            if (item.secondary) {
                labels = Array.isArray(item.secondary) ? item.secondary : [item.secondary];
                secondary = '<br/>' + labels.map(function (label) {
                    return "<span class=\"mdc-list-item__text__secondary\">" + label + "</span>";
                }).join('');
                ulClasses.push('mdc-list--two-line');
            }
            // Did user provide image?
            if (item.image) {
                liClasses.push('uitoolkit-list-li--image');
                pre += "<img class=\"mdc-list-item__start-detail\" src=\"" + item.image + "\"\n\t\t\t\t\t\t\twidth=\"48\" height=\"48\" alt=\"" + (item.image_alt || '') + "\">";
            }
            // Attach the onclick handlers upon creating html element
            if (item.onclick) {
                eventBinders.push(function (elem) {
                    // The browser might not have indexed nodes by id yet, so we need to go by index
                    elem.children[ix].addEventListener('click', item.onclick);
                });
            }
            return "<li class=\"uitoolkit-list-li mdc-list-item " + liClasses.join(' ') + "\" " + attrs.join(' ') + " data-index=\"" + ix + "\">\n\t\t\t\t\t\t" + pre + "\n\t\t\t\t\t\t<span class=\"mdc-list-item__text\">" + primary + secondary + "</span>\n\t\t\t\t\t\t" + post + "\n\t\t\t\t\t</li>";
        }).join('');
        // Bind all onclick events as soon as the element is created
        _this.postExecute = function (elem) {
            eventBinders.forEach(function (binder) { binder(elem); });
        };
        _this.str = "<ul class=\"uitoolkit-list-ul mdc-list " + ulClasses.join(' ') + "\">" + inner + "</ul>";
        return _this;
    }
    return List;
}(component_1.Component));
exports.List = List;
