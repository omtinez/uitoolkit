"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Component = (function () {
    function Component(data) {
        this.str = data.toString();
    }
    Component.id = function (len) {
        if (len === void 0) { len = 8; }
        var key = '';
        for (var i = 0; i < len; i++) {
            key += String.fromCharCode(65 + Math.floor(Math.random() * 26));
        }
        return key;
    };
    Component.prototype.html = function () {
        var d = document;
        var div = d.createElement('div');
        div.innerHTML = this.str.trim();
        var children = [].slice.call(div.childNodes);
        if (this.postExecute)
            children.forEach(this.postExecute);
        return children.length > 1 ? children : children[0];
    };
    Component.prototype.toString = function () {
        return this.str;
    };
    return Component;
}());
exports.Component = Component;
