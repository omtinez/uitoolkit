import { Component } from './component';
/**
 * Linear grid of images in the form of a carouselk. Supports top and bottom text in each tile as
 * well as custom click behavior.
 */
export declare class Carousel extends Component {
    /**
     *
     * @param data List of image URLs or {image: URL, text-top: header, text-bottom: footer}.
     * Optional keys are: {href: URL, alt: text}
     * @param opts
     */
    constructor(data: any[], opts?: any);
}
