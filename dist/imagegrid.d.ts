import { Component } from './component';
/**
 * Grid of images in the form of tiles. Supports top and bottom text in each tile as well as custom
 * click behavior.
 */
export declare class ImageGrid extends Component {
    /**
     * Initialize a Component for a grid of images in the form of tiles.
     * @param data List of image URLs or {image: URL, text-top: header, text-bottom: footer}.
     * Optional keys are: {href: URL, height: pixels}
     * @param opts
     */
    constructor(data: any[], opts?: any);
}
