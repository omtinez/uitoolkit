var assert = require('assert');
var UIToolkit = require('../').UIToolkit;

// Reusable function to test for number of carousel elements and if each of the
// images described in `arr` appear exactly once in the output
function testArr2Elem(arr, elem) {
    const html = elem.toString();
    assert.equal(arr.length, html.match(/<img[\s\S]+?\/>/g).length);
    assert.equal(arr.length, html.match(/[ "]uitoolkit-carousel--wrapper[ "]/g).length);
    arr.forEach(function testItem(item) {
        if (typeof item === 'string') {
            assert.equal(1, html.match(new RegExp(item, 'g')).length);
        } else if (Array.isArray(item)) {
            assert.equal(1, html.match(new RegExp(item[0], 'g')).length);
            if (item.length > 1) assert.equal(1, html.match(new RegExp(item[1], 'g')).length);
        } else {
            if (item.img) testItem(item.img);
            if (item['text-top']) testItem(item['text-top']);
            if (item['text-bottom']) testItem(item['text-bottom']);
        }
    });
}

describe('Carousel', function() {

    it('Carousel of 6 Images', function() {
        const arr = [
            '//placehold.it/250/29e',
            '//placehold.it/250/2e9',
            '//placehold.it/250/9e2',
            '//placehold.it/250/92e',
            '//placehold.it/250/e29',
            '//placehold.it/250/e92'
        ];
        const elem = new UIToolkit.Carousel(arr);
        testArr2Elem(arr, elem);
    });

    it('Carousel of 6 Clickable Images', function() {
        const arr = [
            {image: '//placehold.it/250/29e', href: 'javascript:void(0);'},
            {image: '//placehold.it/250/2e9', href: 'javascript:void(0);'},
            {image: '//placehold.it/250/9e2', href: 'javascript:void(0);'},
            {image: '//placehold.it/250/92e', href: 'javascript:void(0);'},
            {image: '//placehold.it/250/e29', href: 'javascript:void(0);'},
            {image: '//placehold.it/250/e92', href: 'javascript:void(0);'},
        ];
        const elem = new UIToolkit.Carousel(arr);
        testArr2Elem(arr, elem);
    });
    
        it('Carousel of 6 Images with Top Text', function() {
        const arr = [
            {image: '//placehold.it/250/29e', 'text-top': 'blue'},
            {image: '//placehold.it/250/2e9', 'text-top': 'red'},
            {image: '//placehold.it/250/9e2', 'text-top': 'green'},
            {image: '//placehold.it/250/92e', 'text-top': 'yellow'},
            {image: '//placehold.it/250/e29', 'text-top': 'magenta'},
            {image: '//placehold.it/250/e92', 'text-top': 'olive'}
        ];
        const elem = new UIToolkit.Carousel(arr);
        testArr2Elem(arr, elem);
    });

    it('Carousel of 6 Images with Bottom Text', function() {
        const arr = [
            {image: '//placehold.it/250/29e', 'text-bottom': 'banana'},
            {image: '//placehold.it/250/2e9', 'text-bottom': 'apple'},
            {image: '//placehold.it/250/9e2', 'text-bottom': 'watermelon'},
            {image: '//placehold.it/250/92e', 'text-bottom': 'orange'},
            {image: '//placehold.it/250/e29', 'text-bottom': 'kiwi'},
            {image: '//placehold.it/250/e92', 'text-bottom': 'grape'}
        ];
        const elem = new UIToolkit.Carousel(arr);
        testArr2Elem(arr, elem);
    });

    it('Carousel of 6 Images with Top and Bottom Text', function() {
        const arr = [
            {image: '//placehold.it/250/29e', 'text-top': 'blue', 'text-bottom': 'banana'},
            {image: '//placehold.it/250/2e9', 'text-top': 'red', 'text-bottom': 'apple'},
            {image: '//placehold.it/250/9e2', 'text-top': 'green', 'text-bottom': 'watermelon'},
            {image: '//placehold.it/250/92e', 'text-top': 'yellow', 'text-bottom': 'orange'},
            {image: '//placehold.it/250/e29', 'text-top': 'magenta', 'text-bottom': 'kiwi'},
            {image: '//placehold.it/250/e92', 'text-top': 'olive', 'text-bottom': 'grape'}
        ];
        const elem = new UIToolkit.Carousel(arr);
        testArr2Elem(arr, elem);
    });

    it('Carousel of 6 Images with Top and Bottom Text', function() {
        const arr = [
            {image: '//placehold.it/250/29e', 'text-top': 'blue', 'text-bottom': 'banana'},
            {image: '//placehold.it/250/2e9', 'text-top': 'red', 'text-bottom': 'apple'},
            {image: '//placehold.it/250/9e2', 'text-top': 'green', 'text-bottom': 'watermelon'},
            {image: '//placehold.it/250/92e', 'text-top': 'yellow', 'text-bottom': 'orange'},
            {image: '//placehold.it/250/e29', 'text-top': 'magenta', 'text-bottom': 'kiwi'},
            {image: '//placehold.it/250/e92', 'text-top': 'olive', 'text-bottom': 'grape'}
        ];
        const elem = new UIToolkit.Carousel(arr);
        testArr2Elem(arr, elem);
    });

    it('Carousel of 6 Images with Long Text and Clickable', function() {
        const arr = [
            {image: '//placehold.it/250/29e', href: 'javascript;void(0);', 'text-top': 'blue blue blue blue blue blue blue blue blue blue '},
            {image: '//placehold.it/250/2e9', href: 'javascript;void(0);', 'text-top': 'red red red red red red red red red red '},
            {image: '//placehold.it/250/9e2', href: 'javascript;void(0);', 'text-top': 'green green green green green green green green green green '},
            {image: '//placehold.it/250/92e', href: 'javascript;void(0);', 'text-top': 'yellow yellow yellow yellow yellow yellow yellow yellow yellow yellow '},
            {image: '//placehold.it/250/e29', href: 'javascript;void(0);', 'text-top': 'magenta magenta magenta magenta magenta magenta magenta magenta magenta magenta '},
            {image: '//placehold.it/250/e92', href: 'javascript;void(0);', 'text-top': 'olive olive olive olive olive olive olive olive olive olive '}
        ];
        const elem = new UIToolkit.Carousel(arr);
        testArr2Elem(arr, elem);
    });


}); 
