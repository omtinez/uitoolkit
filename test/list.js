var assert = require('assert');
var UIToolkit = require('../').UIToolkit;

// Reusable function to test for number of <li> elements and if each
// of the strings described in `arr` appear exactly once in the output
function testArr2List(arr, list) {
    const html = list.toString();
    assert.equal(arr.length, html.match(/<\/li>/g).length);
    arr.forEach(function testItem(item) {
        if (typeof item === 'string') {
            assert.equal(1, html.match(new RegExp(item, 'g')).length);
        } else if (Array.isArray(item)) {
            assert.equal(1, html.match(new RegExp(item[0], 'g')).length);
            if (item.length > 1) assert.equal(1, html.match(new RegExp(item[1], 'g')).length);
        } else {
            if (item.primary) testItem(item.primary);
            if (item.secondary) testItem(item.secondary);
        }
    });
}

describe('List', function() {

    it('Simple List', function() {
        const arr = ['John', 'Peter', 'Mark'];
        const list = new UIToolkit.List(arr);
        testArr2List(arr, list);
    });

    it('Two-Line Primary List', function() {
        const arr1 = [['John', 'Blacksmith'], ['Peter', 'Writer'], ['Mark', 'Soldier']];
        const arr2 = arr1.map(function(labels) {
            return { primary: labels };
        });
        const list1 = new UIToolkit.List(arr1);
        const list2 = new UIToolkit.List(arr2);
        testArr2List(arr1, list1);
        testArr2List(arr2, list2);
        assert.equal(list1.toString(), list2.toString());
    });

    it('Two-Line Dense List', function() {
        const arr = [['John', 'Blacksmith'], ['Peter', 'Writer'], ['Mark', 'Soldier']];
        const list1 = new UIToolkit.List(arr);
        const list2 = new UIToolkit.List(arr, {dense: true});
        testArr2List(arr, list1);
        testArr2List(arr, list2);
        assert.notEqual(list1.toString(), list2.toString());
    });

    it('Two-Line Primary-Secondary List', function() {
        const arr1 = [['John', 'Blacksmith'], ['Peter', 'Writer'], ['Mark', 'Soldier']];
        const arr2 = arr1.map(function(labels) {
            return { primary: labels[0], secondary: labels[1] };
        });
        const list1 = new UIToolkit.List(arr1);
        const list2 = new UIToolkit.List(arr2);
        testArr2List(arr1, list1);
        testArr2List(arr2, list2);
        assert.notEqual(list1.toString(), list2.toString());
    });

    it('List with Images', function() {
        const arr = [
        	{primary: 'John', secondary: 'Blacksmith', image: '//placehold.it/48/aee'},
        	{primary: 'Peter', secondary: 'Writer', image: '//placehold.it/48/aee'},
        	{primary: 'Mark', secondary: 'Soldier', image: '//placehold.it/48/aee'}
        ];
        const list = new UIToolkit.List(arr);
        testArr2List(arr, list);
    });

    it('List with Avatars', function() {
        const arr = [
        	{primary: 'John', secondary: 'Blacksmith', image: '//placehold.it/48/aee'},
        	{primary: 'Peter', secondary: 'Writer', image: '//placehold.it/48/aee'},
        	{primary: 'Mark', secondary: 'Soldier', image: '//placehold.it/48/aee'}
        ];
        const list1 = new UIToolkit.List(arr);
        const list2 = new UIToolkit.List(arr, {avatars: true});
        testArr2List(arr, list1);
        testArr2List(arr, list2);
        assert.notEqual(list1.toString(), list2.toString());
    });

    it('Clickable List', function() {
        const arr = [
        	{primary: 'John', secondary: 'Blacksmith', image: '//placehold.it/48/aee'},
        	{primary: 'Peter', secondary: 'Writer', image: '//placehold.it/48/aee'},
        	{primary: 'Mark', secondary: 'Soldier', image: '//placehold.it/48/aee'}
        ];
        const list1 = new UIToolkit.List(arr);
        const list2 = new UIToolkit.List(arr, {avatars: true});
        testArr2List(arr, list1);
        testArr2List(arr, list2);
        assert.notEqual(list1.toString(), list2.toString());
    });
    
    
    it('List Indexes', function() {
        const arr = ['John', 'Peter', 'Mark'];
        const list = new UIToolkit.List(arr);
        const out = list.toString();
        arr.forEach(function(item, ix) {
            assert.equal(1, out.match(new RegExp(`data-index="${ix}"`, 'g')).length);
        });
    });


}); 
