var assert = require('assert');
var UIToolkit = require('../').UIToolkit;

// Reusable function to test for number of input elements and if each
// of the values given appear exactly once in the output
function testArr2Out(arr, out) {
    assert.equal(arr.length, out.match(/<((input)|(textarea))[\s\S]+?\/?>/g).length);
    arr.forEach(function testItem(item) {
        if (typeof item === 'string') {
            assert.equal(1, out.match(new RegExp(item, 'g')).length);
        } else if (Array.isArray(item)) {
            item.forEach(function(item_) {
            	assert.equal(1, out.match(new RegExp(item_, 'g')).length);
            });
        } else {
            if (item.id) testItem(`id="${item.id}"`);
            if (item.name) testItem(`name="${item.name}"`);
            if (item.hint) testItem(`placeholder="${item.hint}"`);
            if (item.pattern) testItem(`pattern="${item.pattern}"`);
            if (item.autocomplete) testItem(`autocomplete="${item.autocomplete}"`);
            // TODO: test custom classes and styles too
        }
    });
}

// Helper function to remove references to ID from element strings
function stripId(out) {
    out.match(/id=\"(.+?)\"/g).forEach(function(id) {
        out = out
            .replace(new RegExp(id, 'g'), '')
            .replace(new RegExp(id.replace('id', 'for'), 'g'), '');
    })
    return out;
}

describe('Textfields', function() {

    it('Single text field', function() {
        const arr = [
            'Batman'
        ];
        const elem = new UIToolkit.Textfields(arr);
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elem.toString());
    });

    it('Single multiline text field', function() {
        const arr = ['Batman'];
        const obj = [{name: 'Batman', multiline: true}];
        const elem1 = new UIToolkit.Textfields(arr, {multiline: true});
        const elem2 = new UIToolkit.Textfields(obj);
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elem1.toString());
        assert.equal(stripId(elem1.toString()), stripId(elem2.toString()));
    });

    it('Multiple text fields', function() {
        const arr = [
            'Batman',
            'Robin',
            'Superman',
            'Captain America',
            'Hulk'
        ];
        const elems = new UIToolkit.Textfields(arr);
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elems.toString());
    });

    it('Multiple multiline text fields', function() {
        const arr = [
            'Batman',
            'Robin',
            'Superman'
        ];
        const obj = [
            {name: 'Batman', multiline: true},
            {name: 'Robin', multiline: true},
            {name: 'Superman', multiline: true}
        ];
        const elem1 = new UIToolkit.Textfields(arr, {multiline: true});
        const elem2 = new UIToolkit.Textfields(obj);
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elem1.toString());
        assert.equal(stripId(elem1.toString()), stripId(elem2.toString()));
    });

    it('Multiple text fields with no hint', function() {
        const arr = [
            'Batman',
            'Robin',
            'Superman',
            'Captain America',
            'Hulk'
        ];
        const elems = new UIToolkit.Textfields(arr, {nohint: true});
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elems.toString());
    });

    it('Multiple multiline text fields with no hint', function() {
        const arr = [
            'Batman',
            'Robin',
            'Superman'
        ];
        const obj = [
            {name: 'Batman', multiline: true},
            {name: 'Robin', multiline: true},
            {name: 'Superman', multiline: true}
        ];
        const elem1 = new UIToolkit.Textfields(arr, {nohint: true, multiline: true});
        const elem2 = new UIToolkit.Textfields(obj, {nohint: true});
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elem1.toString());
        assert.equal(stripId(elem1.toString()), stripId(elem2.toString()));
    });

    it('Multiple text fields without Labels', function() {
        const arr = [
            'Batman',
            'Robin',
            'Superman',
            'Captain America',
            'Hulk'
        ];
        const elems = new UIToolkit.Textfields(arr, {nohint: true, nolabel: true});
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elems.toString());
    });

    it('Multiple Multiline text fields without labels', function() {
        const arr = [
            'Batman',
            'Robin',
            'Superman'
        ];
        const obj = [
            {name: 'Batman', multiline: true},
            {name: 'Robin', multiline: true},
            {name: 'Superman', multiline: true}
        ];
        const elem1 = new UIToolkit.Textfields(arr, {nohint: true, nolabel: true, multiline: true});
        const elem2 = new UIToolkit.Textfields(obj, {nohint: true, nolabel: true});
        testArr2Out(arr.map((item) => 'name="' + item + '"'), elem1.toString());
        assert.equal(stripId(elem1.toString()), stripId(elem2.toString()));
    });

    it('Autocomplete', function() {
        const obj = [
            {name: 'Batman', autocomplete: 'given-name'},
        ];
        const elem = new UIToolkit.Textfields(obj);
        testArr2Out(obj, elem.toString());
    });

    // TODO: test required, id, pattern

}); 
