var assert = require('assert');
var UIToolkit = require('../').UIToolkit;

// Reusable function to test for number of <li> elements and if each
// of the strings described in `arr` appear exactly once in the output
function testArr2Out(arr, out) {
    if (!Array.isArray(arr)) arr = Object.keys(arr).map((key) => arr[key]);
    arr.forEach(function testItem(item) {
        if (!Array.isArray(item)) {
        	assert.equal(1, out.match(new RegExp(item, 'g')).length);
        }
    });
}

describe('Card', function() {

    it('Simple Card', function() {
        const arr = ['Hello World'];
        const obj = {title: 'Hello World'};
        const elem1 = new UIToolkit.Card(arr);
        const elem2 = new UIToolkit.Card(obj);
        testArr2Out(arr, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Card with text content', function() {
        const arr = ['Hello World', 'Supporting text'];
        const obj = {title: 'Hello World', content: 'Supporting text'};
        const elem1 = new UIToolkit.Card(arr);
        const elem2 = new UIToolkit.Card(obj);
        testArr2Out(arr, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Card with html content', function() {
        const arr = ['Hello World', 'Supporting <b>text</b>'];
        const obj = {title: 'Hello World', content: 'Supporting <b>text</b>'};
        const elem1 = new UIToolkit.Card(arr);
        const elem2 = new UIToolkit.Card(obj);
        testArr2Out(arr, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Card with text content and subtitle', function() {
        const arr = ['Hello World', 'Supporting text', 'Subtitle here'];
        const obj = {title: 'Hello World', content: 'Supporting text', subtitle: 'Subtitle here'};
        const elem1 = new UIToolkit.Card(arr);
        const elem2 = new UIToolkit.Card(obj);
        testArr2Out(arr, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Card with media', function() {
        const obj = {title: 'Hello World', image: '//placehold.it/600x90'};
        const elem = new UIToolkit.Card(obj);
        testArr2Out(obj, elem.toString());
    });

    it('Dismissable card', function() {
        const obj = {title: 'Hello World', content: 'Click OK to dismiss'};
        const elem = new UIToolkit.Card(obj, {autodismiss: true});
        testArr2Out(obj, elem.toString());
    });

    it('Card with custom actions', function() {
        var actions = [{
            label: 'Action 1',
            onclick: function(e) { console.log('User clicked Action 1'); }
        }, {
            label: 'Action 2',
            onclick: function(e) { console.log('User clicked Action 2'); }
        }];
        const obj = {title: 'Hello World', content: 'Click a button below', actions: actions};
        const elem = new UIToolkit.Card(obj);
        testArr2Out(obj, elem.toString());
    });

}); 
