var assert = require('assert');
var UIToolkit = require('../').UIToolkit;

// Reusable function to test for number of input elements and if each
// of the labels given appear exactly once in the output
function testArr2Out(arr, out) {
    assert.equal(arr.length, out.match(/<input[\s\S]+?\/>/g).length);
    arr.forEach(function testItem(item) {
        if (typeof item === 'string') {
            assert.equal(1, out.match(new RegExp('[^\"]' + item + '[^\"]', 'g')).length);
        } else if (Array.isArray(item)) {
            item.forEach(function(item_) {
            	assert.equal(1, out.match(new RegExp('[^\"]' + item_ + '[^\"]', 'g')).length);
            });
        } else {
            if (item.id) testItem(`id="${item.id}"`);
            if (item.label) testItem(`label="${item.label}"`);
            if (item.value) testItem(`value="${item.value}"`);
        }
    });
}

describe('Radios', function() {

    it('Single Radio Button', function() {
        const arr = [
            'Batman'
        ];
        const elem = new UIToolkit.Radios(arr);
        testArr2Out(arr, elem.toString());
    });

    it('Multiple Radio Buttons', function() {
        const arr = [
            'Batman',
            'Robin',
            'Superman',
            'Captain America',
            'Hulk'
        ];
        const elems = new UIToolkit.Radios(arr);
        testArr2Out(arr, elems.toString());
    });

    it('Multiple Radio Buttons Without Labels', function() {
        var arr = [
            {value: 1},
            {value: 2},
            {value: 3},
            {value: 4},
            {value: 5}
        ];
        var elems = new UIToolkit.Radios(arr, {name: 'numbers'});
        testArr2Out(arr, elems.toString());
    });
    
    // TODO: test required, id

}); 
