var assert = require('assert');
var UIToolkit = require('../').UIToolkit;

// Reusable function to test for the content of each label in the output
function testArr2Out(arr, out) {
    assert.equal(arr.length, out.match(/<\/button>/g).length);
    arr.forEach(function testItem(item) {
        if (Array.isArray(item)) {
            assert.equal(1, out.match(new RegExp(item[0], 'g')).length);
        } else if (typeof item === 'object') {
            assert.equal(1, out.match(new RegExp(item.label, 'g')).length);
        } else {
        	assert.equal(1, out.match(new RegExp(item, 'g')).length);
        }
    });
}

describe('Buttons', function() {

    it('Simple Button', function() {
        const arr = ['Hello World'];
        const obj = [{label: 'Hello World'}];
        const elem1 = new UIToolkit.Buttons(arr);
        const elem2 = new UIToolkit.Buttons(obj);
        testArr2Out(arr, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Borderless Button', function() {
        const obj1 = [{label: 'Hello World'}];
        const obj2 = [{label: 'Hello World', borderless: true}];
        const elem1 = new UIToolkit.Buttons(obj1, {borderless: true});
        const elem2 = new UIToolkit.Buttons(obj2);
        testArr2Out(obj1, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Colored Button', function() {
        const obj1 = [{label: 'Hello World'}];
        const obj2 = [{label: 'Hello World', colored: true}];
        const elem1 = new UIToolkit.Buttons(obj1, {colored: true});
        const elem2 = new UIToolkit.Buttons(obj2);
        testArr2Out(obj1, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Raised Button', function() {
        const obj1 = [{label: 'Hello World'}];
        const obj2 = [{label: 'Hello World', raised: true}];
        const elem1 = new UIToolkit.Buttons(obj1, {raised: true});
        const elem2 = new UIToolkit.Buttons(obj2);
        testArr2Out(obj1, elem1.toString());
        assert.equal(elem1.toString(), elem2.toString());
    });

    it('Multiple Buttons', function() {
        const arr = ['Button 1', 'Button 2', 'Button 3'];
        const elem = new UIToolkit.Buttons(arr);
        testArr2Out(arr, elem.toString());
    });

    it('Button with custom actions', function() {
        const obj = [{
            label: 'Hello World',
            onclick: function(e) {
                console.log('User clicked button');
            }
        }];
        const elem = new UIToolkit.Buttons(obj);
        testArr2Out(obj, elem.toString());
    });

}); 
